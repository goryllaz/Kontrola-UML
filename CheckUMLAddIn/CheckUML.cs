﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EA;

namespace CheckUMLAddIn
{
    public class CheckUML: EAAddinFramework.EAAddinBase
    {

        protected override string menuHeader { get; set; } = "-&Kontrola UML";
        protected override string[] menuOptions { get; set; } = {"&Spustit"};

        public override void EA_MenuClick(Repository Repository, string MenuLocation, string MenuName, string ItemName)
        {
            if (ItemName == menuOptions[0])
            {
                if (IsProjectOpen(Repository))
                    Application.Run(new Form1(Repository));
                else
                    MessageBox.Show("Soubor musí být otevřený.");
            }
        }
    }
}
