﻿using System.Drawing;

namespace CheckUMLAlerts
{
    public interface IAlert
    {
        Image Icon { get; }
        string Report { get; set; }
    }
}
