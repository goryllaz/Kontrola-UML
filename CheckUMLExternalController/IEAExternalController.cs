﻿using System.Collections.Generic;
using CheckUMLAlerts;
using EA;

namespace CheckUMLExternalController
{
    public interface IEAExternalController
    {
        Repository Repository { get; set; }
        List<IAlert> GetResults();
    }
}
