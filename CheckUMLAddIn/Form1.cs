﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KontrolaUML;

namespace CheckUMLAddIn
{
    public partial class Form1 : Form
    {

        private EAHandler EAHander { get; set; }
        public EA.Repository Repository { get; set; }

        public Form1(EA.Repository repository)
        {
            Repository = repository;
            InitializeComponent();
        }

        private void checkBtn_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            EAHander = new EAHandler();
            EAHander.CheckProject(Repository);
            Cursor.Current = Cursors.Default;
            alertsTable.DataSource = EAHander.Alerts;
            chooseDiagram.Enabled = true;

        }

        private void chooseDiagram_Click(object sender, EventArgs e)
        {
            Dialog dialog = new Dialog(EAHander.EADiagrams);
            dialog.TopLevel = true;
            Enabled = false;
            dialog.Closed += CloseDialog;
            dialog.Show();
        }

        private void CloseDialog(object sender, EventArgs e)
        {
            Enabled = true;
            Cursor.Current = Cursors.WaitCursor;
            EAHander.CheckAllDiagrams(true);
            Cursor.Current = Cursors.Default;
            alertsTable.DataSource = EAHander.Alerts;
        }

        private void Form1Resize(object sender, EventArgs e)
        {
            chooseDiagram.Location = new System.Drawing.Point(Width - 114, 13);
            alertsTable.Width = Width - 40;
            alertsTable.Height = Height - 116;
            checkBtn.Location = new System.Drawing.Point(Width / 2 - (checkBtn.Width / 2), 13);
        }

        private void AlertsTableResize(object sender, EventArgs e)
        {
            if (alertsTable.Columns.Count >= 2)
            {
                int alertsTableMinWidth = alertsTable.Width - 22 -
                                          SystemInformation.VerticalScrollBarWidth - 3;
                if (alertsTable.Columns[1].MinimumWidth < alertsTableMinWidth)
                {
                    alertsTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                    alertsTable.Columns[1].Width = alertsTableMinWidth;
                }
                else
                {
                    alertsTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                }
            }
        }

        private void AlertsTableLoading(object sender, EventArgs e)
        {
            if (alertsTable.Columns.Count >= 2)
            {
                alertsTable.Columns[0].ReadOnly = true;
                alertsTable.Columns[1].ReadOnly = true;
                alertsTable.Columns[1].MinimumWidth = alertsTable.Columns[1].Width;
                alertsTable.Columns[0].MinimumWidth = 22;
                alertsTable.Columns[0].Width = 22;
            }
        }
    }
}
