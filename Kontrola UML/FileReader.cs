﻿using System.IO;

namespace KontrolaUML
{
    class FileReader
    {
        private string _path;

        public string Path
        {
            get
            {
                return _path;
            }
            set
            {
                if (File.Exists(value))
                {
                    _path = value;
                }
                else
                {
                    throw new FileNotFoundException(value);
                }
            }
        }


    }
}
