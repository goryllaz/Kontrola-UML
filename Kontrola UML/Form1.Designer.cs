﻿using System;
using System.Windows.Forms;

namespace KontrolaUML
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectBtn = new System.Windows.Forms.Button();
            this.selectFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.filePath = new System.Windows.Forms.TextBox();
            this.checkBtn = new System.Windows.Forms.Button();
            this.chooseDiagram = new System.Windows.Forms.Button();
            this.alertsTable = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.alertsTable)).BeginInit();
            this.SuspendLayout();
            // 
            // selectBtn
            // 
            this.selectBtn.Location = new System.Drawing.Point(12, 13);
            this.selectBtn.Name = "selectBtn";
            this.selectBtn.Size = new System.Drawing.Size(75, 23);
            this.selectBtn.TabIndex = 0;
            this.selectBtn.Text = "Vybrat";
            this.selectBtn.UseVisualStyleBackColor = true;
            this.selectBtn.Click += new System.EventHandler(this.selectBtn_Click);
            // 
            // selectFileDialog
            // 
            this.selectFileDialog.Filter = "*.eap|*.EAP";
            this.selectFileDialog.Title = "Vyberte soubor Enterprise Architect";
            // 
            // filePath
            // 
            this.filePath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.filePath.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.filePath.Enabled = false;
            this.filePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.filePath.Location = new System.Drawing.Point(94, 16);
            this.filePath.Name = "filePath";
            this.filePath.ReadOnly = true;
            this.filePath.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.filePath.Size = new System.Drawing.Size(349, 14);
            this.filePath.TabIndex = 1;
            // 
            // checkBtn
            // 
            this.checkBtn.Enabled = false;
            this.checkBtn.Location = new System.Drawing.Point(198, 36);
            this.checkBtn.Name = "checkBtn";
            this.checkBtn.Size = new System.Drawing.Size(75, 23);
            this.checkBtn.TabIndex = 2;
            this.checkBtn.Text = "Zkontrolovat";
            this.checkBtn.UseVisualStyleBackColor = true;
            this.checkBtn.Click += new System.EventHandler(this.checkBtn_Click);
            // 
            // chooseDiagram
            // 
            this.chooseDiagram.Enabled = false;
            this.chooseDiagram.Location = new System.Drawing.Point(357, 36);
            this.chooseDiagram.Name = "chooseDiagram";
            this.chooseDiagram.Size = new System.Drawing.Size(86, 23);
            this.chooseDiagram.TabIndex = 3;
            this.chooseDiagram.Text = "Zvolit diagramy";
            this.chooseDiagram.UseVisualStyleBackColor = true;
            this.chooseDiagram.Click += new System.EventHandler(this.chooseDiagram_Click);
            // 
            // alertsTable
            // 
            this.alertsTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            this.alertsTable.Location = new System.Drawing.Point(12, 65);
            this.alertsTable.Name = "alertsTable";
            this.alertsTable.Size = new System.Drawing.Size(431, 194);
            this.alertsTable.TabIndex = 4;
            this.alertsTable.RowHeadersVisible = false;
            this.alertsTable.ColumnHeadersVisible = false;
            this.alertsTable.AllowUserToResizeRows = false;
            this.alertsTable.AllowUserToResizeColumns = false;
            this.alertsTable.RowTemplate.MinimumHeight = 22;
            this.alertsTable.RowTemplate.Height = 22;
            this.alertsTable.DataBindingComplete += AlertsTableLoading;
            this.alertsTable.DataBindingComplete += AlertsTableResize;
            this.alertsTable.Resize += AlertsTableResize;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //this.ClientSize = new System.Drawing.Size(455, 271);
            this.Controls.Add(this.alertsTable);
            this.Controls.Add(this.chooseDiagram);
            this.Controls.Add(this.checkBtn);
            this.Controls.Add(this.filePath);
            this.Controls.Add(this.selectBtn);
            this.MinimumSize = new System.Drawing.Size(471, 310);
            this.Name = "Form1";
            this.Text = "Kontrola UML diagramů";
            ((System.ComponentModel.ISupportInitialize)(this.alertsTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
            this.Resize += Form1Resize;

        }

        #endregion

        private System.Windows.Forms.Button selectBtn;
        private System.Windows.Forms.OpenFileDialog selectFileDialog;
        private System.Windows.Forms.TextBox filePath;
        private System.Windows.Forms.Button checkBtn;
        private System.Windows.Forms.Button chooseDiagram;
        private System.Windows.Forms.DataGridView alertsTable;
    }
}

