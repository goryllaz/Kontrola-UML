﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace KontrolaUML
{
    public class EALogicalDiagram : IEADiagram
    {
        private readonly EA.Diagram _diagram;
        private readonly List<EA.Element> _elements = new List<EA.Element>();
        private List<EA.Connector>[,] _connectors;

        public string Name => _diagram.Name;
        public int Id => _diagram.DiagramID;
        public Guid Guid => new Guid(_diagram.DiagramGUID);
        public Type Type { get; set; }

        public EALogicalDiagram(EA.Diagram diagram)
        {
            _diagram = diagram;
        }

        public EALogicalDiagram(EA.Diagram diagram, Type type)
        {
            _diagram = diagram;
            Type = type;
        }

        public List<EA.DiagramObject> GetDiagramObjects()
        {
            List<EA.DiagramObject> diagramObjects = new List<EA.DiagramObject>();
            for (short i = 0; i < _diagram.DiagramObjects.Count; i++)
            {
                diagramObjects.Add((EA.DiagramObject) _diagram.DiagramObjects.GetAt(i));
            }
            return diagramObjects;
        }

        public void AddElement(EA.Element element)
        {
            _elements.Add(element);
        }

        public EA.Element GetElement(int i)
        {
            return _elements[i];
        }

        [Browsable(false)]
        public int CountElements => _elements.Count;

        public void CreateConnectionsArrayList()
        {
            _connectors = new List<EA.Connector>[CountElements, CountElements];
        }

        public void AddConnectors(List<EA.Connector> connector, int index0, int index1)
        {
            _connectors[index0, index1] = connector;
        }

        public List<EA.Connector> GetConnectors(int index0, int index1)
        {
            return _connectors[index0, index1];
        }
    }
}
