﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CheckUMLAlerts;

namespace KontrolaUML
{
    static class EAController
    {
        private static readonly string TxtClass = "třídy";
        private static readonly string TxtInterface = "rozhraní";
        private static readonly string TxtPackage = "balíčku";
        private static readonly string TxtElement = "elementu";
        private static readonly string TxtAttribute = "atributu";
        private static readonly string TxtMethod = "metody";

        private static readonly string TxtClass2 = "třídou";
        private static readonly string TxtInterface2 = "rozhraním";
        private static readonly string TxtPackage2 = "balíčkem";
        private static readonly string TxtElement2 = "elementem";

        private static readonly string TxtAssociation = "asociace";
        private static readonly string TxtAggregation = "agregace";
        private static readonly string TxtComposition = "kompozice";
        private static readonly string TxtRealisation = "realizace";
        private static readonly string TxtGeneralisation = "generalizace";

        private static readonly string EClass = "class";
        private static readonly string EInterface = "interface";
        private static readonly string EPackage = "package";

        private static readonly string CAssociation = "association";
        private static readonly string CAggregation = "aggregation";
        private static readonly string CRealisation = "realisation";
        private static readonly string CGeneralisation = "generalisation";

        private static string GetElementTxt(EA.Element element)
        {
            if (element.Type.ToLower() == EClass)
                return TxtClass;
            if (element.Type.ToLower() == EInterface)
                return TxtInterface;
            if (element.Type.ToLower() == EPackage)
                return TxtPackage;
            return TxtElement;
        }

        private static string GetElementTxt2(EA.Element element)
        {
            if (element.Type.ToLower() == EClass)
                return TxtClass2;
            if (element.Type.ToLower() == EInterface)
                return TxtInterface2;
            if (element.Type.ToLower() == EPackage)
                return TxtPackage2;
            return TxtElement2;
        }

        private static bool IsSouserceElement(EA.Element element, EA.Connector connector)
        {
            if (connector.ClientID == element.ElementID)
                return true;
            return false;
        }

        /*
         * CamelCase
         */
        private static List<EAStatus.Text> CheckCamelCase(string text)
        {
            List<EAStatus.Text> statuses = new List<EAStatus.Text>();

            if (text.IndexOf(" ", StringComparison.Ordinal) > 0)
                statuses.Add(EAStatus.Text.WhiteSpace);

            if (text.Length > 2 && !Regex.IsMatch(text.Substring(1, text.Length-2), "^[\\p{L}0-9\\s\\.]+$"))
                statuses.Add(EAStatus.Text.Delimiter);

            return statuses;
        }

        public static List<IAlert> GetCamelCaseResult(string parent, string elementType, string name, int id)
        {
            List<EAStatus.Text> statuses = CheckCamelCase(name);

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.Text.WhiteSpace)
                    alerts.Add(new Error(
                        $"({parent}) - V názvu {elementType} \"{name}\" s ID č. {id} se nemá objevovat mezera."));
                if (status == EAStatus.Text.Delimiter)
                    alerts.Add(new Warning(
                        $"({parent}) - V názvu {elementType} \"{name}\" s ID č. {id} je symbol který by neměl oddělovat slova."));
            }

            return alerts;
        }

        public static List<IAlert> GetCamelCaseResult(string parent, EA.Element element)
        {
            if (element.Type.ToLower() == EClass)
                return GetCamelCaseResult(parent, TxtClass, element.Name, element.ElementID);
            if (element.Type.ToLower() == EInterface)
                return GetCamelCaseResult(parent, TxtInterface, element.Name, element.ElementID);

            return GetCamelCaseResult(parent, TxtElement, element.Name, element.ElementID);
        }

        public static List<IAlert> GetCamelCaseResult(string parent, EA.Attribute attribute)
        {
            List<IAlert> alerts = GetCamelCaseResult(parent, TxtAttribute, attribute.Name, attribute.AttributeID);
            alerts.AddRange(GetCamelCaseResult(parent, "typu " + TxtAttribute, attribute.Type, attribute.AttributeID));
            return alerts;
        }

        public static List<IAlert> GetCamelCaseResult(string parent, EA.Method method)
        {
            List<IAlert> alerts = GetCamelCaseResult(parent, TxtMethod, method.Name, method.MethodID);
            alerts.AddRange(GetCamelCaseResult(parent, "návratového typu " + TxtMethod, method.ReturnType, method.MethodID));
            return alerts;
        }

        /*
         * Diacritic
         */
        private static List<EAStatus.Text> CheckTextDiacritic(string text)
        {
            List<EAStatus.Text> statuses = new List<EAStatus.Text>();

            if (text.Length < text.Normalize(System.Text.NormalizationForm.FormD).Length)
                statuses.Add(EAStatus.Text.Diacritic);

            return statuses;
        }

        public static List<IAlert> GetTextDiacriticResult(string parent, string element, string name, int id)
        {
            List<EAStatus.Text> statuses = CheckTextDiacritic(name);

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.Text.Diacritic)
                    alerts.Add(new Error(
                        $"({parent}) - Název {element} \"{name}\" s ID č. {id} nemá obsahovat diakritiku."));
            }
            return alerts;
        }

        public static List<IAlert> GetTextDiacriticResult(string parent, EA.Element element)
        {
            return GetTextDiacriticResult(parent, GetElementTxt(element), element.Name, element.ElementID);
        }

        public static List<IAlert> GetTextDiacriticResult(string parent, EA.Attribute attribute)
        {
            List<IAlert> alerts = GetTextDiacriticResult(parent, TxtAttribute, attribute.Name, attribute.AttributeID);
            alerts.AddRange(GetTextDiacriticResult(parent, "typu " + TxtAttribute, attribute.Type, attribute.AttributeID));
            return alerts;
        }

        public static List<IAlert> GetTextDiacriticResult(string parent, EA.Method method)
        {
            List<IAlert> alerts = GetTextDiacriticResult(parent, TxtMethod, method.Name, method.MethodID);
            alerts.AddRange(GetTextDiacriticResult(parent, "návratového typu " + TxtMethod, method.ReturnType, method.MethodID));
            return alerts;
        }

        /*
         * First uppercase
         */
        private static List<EAStatus.Text> CheckFirstUppercase(string text)
        {
            List<EAStatus.Text> statuses = new List<EAStatus.Text>();
            char[] charactersOfText = text.ToCharArray(0, 2);

            if (char.IsLetter(charactersOfText[0]))
            {
                if (char.IsLower(charactersOfText[0]))
                    statuses.Add(EAStatus.Text.FirstUppercase);
            }
            else if (char.IsLetter(charactersOfText[1]))
            {
                if (char.IsLower(charactersOfText[1]))
                    statuses.Add(EAStatus.Text.FirstUppercase);
            }

            return statuses;
        }

        public static List<IAlert> GetFirstUppercaseResult(string parent, string element, string name, int id)
        {
            List<EAStatus.Text> statuses = CheckFirstUppercase(name);

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.Text.FirstUppercase)
                    alerts.Add(new Warning(
                        $"({parent}) - Název {element} \"{name}\" s ID č. {id} má začínat velkým písemenem."));
            }
            return alerts;
        }

        public static List<IAlert> GetFirstUppercaseResult(string parent, EA.Element element)
        {
            return GetFirstUppercaseResult(parent, GetElementTxt(element), element.Name, element.ElementID);
        }

        public static List<IAlert> GetFirstUppercaseResult(string parent, EA.Attribute attribute)
        {
            return GetFirstUppercaseResult(parent, TxtAttribute, attribute.Name, attribute.AttributeID);
        }

        public static List<IAlert> GetFirstUppercaseResult(string parent, EA.Method method)
        {
            return GetFirstUppercaseResult(parent, TxtMethod, method.Name, method.MethodID);
        }

        /*
         * First lowercase
         */
        private static List<EAStatus.Text> CheckFirstLowercase(string text)
        {
            List<EAStatus.Text> statuses = new List<EAStatus.Text>();
            char[] charactersOfText = text.ToCharArray(0, 2);

            if (char.IsLetter(charactersOfText[0]))
            {
                if (char.IsUpper(charactersOfText[0]))
                    statuses.Add(EAStatus.Text.FirstLowercase);
            }
            else if (char.IsLetter(charactersOfText[1]))
            {
                if (char.IsUpper(charactersOfText[1]))
                    statuses.Add(EAStatus.Text.FirstLowercase);
            }

                return statuses;
        }

        public static List<IAlert> GetFirstLowercaseResult(string parent, string element, string name, int id)
        {
            List<EAStatus.Text> statuses = CheckFirstLowercase(name);

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.Text.FirstLowercase)
                    alerts.Add(new Warning(
                        $"({parent}) - Název {element} \"{name}\" s ID č. {id} má začínat malým písemenem."));
            }
            return alerts;
        }

        public static List<IAlert> GetFirstLowercaseResult(string parent, EA.Attribute attribute)
        {
            return GetFirstLowercaseResult(parent, TxtAttribute, attribute.Name, attribute.AttributeID);
        }

        public static List<IAlert> GetFirstLowercaseResult(string parent, EA.Method method)
        {
            return GetFirstLowercaseResult(parent, TxtMethod, method.Name, method.MethodID);
        }

        /*
         * Include type
         */
        private static List<EAStatus.Include> CheckIncludeType(EA.Attribute attribute)
        {
            List<EAStatus.Include> statuses = new List<EAStatus.Include>();
            if (string.IsNullOrEmpty(attribute.Type))
                statuses.Add(EAStatus.Include.Type);
            return statuses;
        }

        private static List<EAStatus.Include> CheckIncludeType(EA.Method method)
        {
            List<EAStatus.Include> statuses = new List<EAStatus.Include>();
            if (string.IsNullOrEmpty(method.ReturnType) &&
                method.Stereotype.ToLower() != "constructor")
                statuses.Add(EAStatus.Include.Type);
            return statuses;
        }

        public static List<IAlert> GetIncludeTypeResult(string parent, EA.Attribute attribute)
        {
            List<EAStatus.Include> statuses = CheckIncludeType(attribute);

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.Include.Type)
                    alerts.Add(new Error(
                        $"({parent}) - Atribut {attribute.Name} s ID č. {attribute.AttributeID} musí mít definovaný typ atributu."));
            }
            return alerts;
        }

        public static List<IAlert> GetIncludeTypeResult(string parent, EA.Method method)
        {
            List<EAStatus.Include> statuses = CheckIncludeType(method);

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.Include.Type)
                    alerts.Add(new Error(
                        $"({parent}) - Metoda {method.Name} s ID č. {method.MethodID} musí mít definovaný návratoví typ."));
            }
            return alerts;
        }

        /*
         * Include multiplicity
         */
        private static List<EAStatus.Include> CheckIcludeMultiplicity(EA.ConnectorEnd connectorEnd)
        {
            List<EAStatus.Include> statuses = new List<EAStatus.Include>();
            if (string.IsNullOrEmpty(connectorEnd.Cardinality))
                statuses.Add(EAStatus.Include.Multiplicity);
            return statuses;
        }

        public static List<IAlert> GetIcludeMultiplicityResult(string parent, EA.Element element1, EA.Element element2, EA.Connector connector)
        {
            List<EAStatus.Include> statuses = CheckIcludeMultiplicity(connector.ClientEnd);
            List<EAStatus.Include> statuses2 = CheckIcludeMultiplicity(connector.SupplierEnd);
            if (!IsSouserceElement(element1, connector))
            {
                EA.Element tmp = element1;
                element1 = element2;
                element2 = tmp;
            }

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.Include.Multiplicity)
                    alerts.Add(new Error(
                        $"({parent}) - Relace s ID č. {connector.ConnectorID} mezi {GetElementTxt2(element1)} {element1.Name} a {GetElementTxt2(element2)} {element2.Name} musí obsahovat multiplicitu u zdrojového elementu {element1.Name}."));
            }
            foreach (var status in statuses2)
            {
                if (status == EAStatus.Include.Multiplicity)
                    alerts.Add(new Error(
                        $"({parent}) - Relace s ID č. {connector.ConnectorID} mezi {GetElementTxt2(element1)} {element1.Name} a {GetElementTxt2(element2)} {element2.Name} musí obsahovat multiplicitu u cílového elementu {element2.Name}."));
            }
            return alerts;
        }

        /*
         * Not include multiplicity
         */
        private static List<EAStatus.NotInclude> CheckNotIcludeMultiplicity(EA.ConnectorEnd connectorEnd)
        {
            List<EAStatus.NotInclude> statuses = new List<EAStatus.NotInclude>();
            if (!string.IsNullOrEmpty(connectorEnd.Cardinality))
                statuses.Add(EAStatus.NotInclude.Multiplicity);
            return statuses;
        }

        public static List<IAlert> GetNotIncludeMultiplicityResult(string parent, EA.Element element1, EA.Element element2, EA.Connector connector)
        {
            List<EAStatus.NotInclude> statuses = CheckNotIcludeMultiplicity(connector.ClientEnd);
            List<EAStatus.NotInclude> statuses2 = CheckNotIcludeMultiplicity(connector.SupplierEnd);
            if (!IsSouserceElement(element1, connector))
            {
                EA.Element tmp = element1;
                element1 = element2;
                element2 = tmp;
            }


            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.NotInclude.Multiplicity)
                    alerts.Add(new Error(
                        $"({parent}) - Relace s ID č. {connector.ConnectorID} mezi {GetElementTxt2(element1)} {element1.Name} a {GetElementTxt2(element2)} {element2.Name} nemá obsahovat multiplicitu {connector.ClientEnd.Cardinality} u zdrojového elementu {element1.Name}."));
            }
            foreach (var status in statuses2)
            {
                if (status == EAStatus.NotInclude.Multiplicity)
                    alerts.Add(new Error(
                        $"({parent}) - Relace s ID č. {connector.ConnectorID} mezi {GetElementTxt2(element1)} {element1.Name} a {GetElementTxt2(element2)} {element2.Name} nemá obsahovat multiplicitu {connector.SupplierEnd.Cardinality} u cílového elementu {element2.Name}."));
            }
            return alerts;
        }

        /*
         * Not include attribute
         */
        private static List<EAStatus.NotInclude> CheckNotIncludeAttribute(EA.Element element)
        {
            List<EAStatus.NotInclude> statuses = new List<EAStatus.NotInclude>();
            if (element.Attributes.Count > 0)
                statuses.Add(EAStatus.NotInclude.Attributes);
            return statuses;
        }

        public static List<IAlert> GetNotIncludeAttributeResult(string parent, EA.Element element)
        {
            List<EAStatus.NotInclude> statuses = CheckNotIncludeAttribute(element);

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.NotInclude.Attributes)
                    alerts.Add(new Error(
                        $"({parent}) - Součástí {GetElementTxt(element)} {element.Name} s ID č. {element.ElementID} nemají být atributy."));
            }
            return alerts;
        }

        /*
         * Not iclude connector
         */
        private static List<EAStatus.NotInclude> CheckNotIncludeConnector(EA.Element element, EA.Connector connector)
        {
            List<EAStatus.NotInclude> statuses = new List<EAStatus.NotInclude>();
            string eType = element.Type.ToLower();
            string cType = connector.Type.ToLower();

            if (eType == EInterface && cType == CAssociation)
                statuses.Add(EAStatus.NotInclude.Association);
            if (eType == EInterface && cType == CAggregation &&
                connector.SupplierEnd.Aggregation == 1)
                statuses.Add(EAStatus.NotInclude.Aggregation);
            if (eType == EInterface && cType == CAggregation &&
                connector.SupplierEnd.Aggregation == 2)
                statuses.Add(EAStatus.NotInclude.Composition);
            if (eType == EInterface && cType == CGeneralisation)
                statuses.Add(EAStatus.NotInclude.Generalisation);
            
            if (eType == EPackage && cType == CAssociation)
                statuses.Add(EAStatus.NotInclude.Association);
            if (eType == EPackage && cType == CAggregation &&
                connector.SupplierEnd.Aggregation == 1)
                statuses.Add(EAStatus.NotInclude.Aggregation);
            if (eType == EPackage && cType == CAggregation &&
                connector.SupplierEnd.Aggregation == 2)
                statuses.Add(EAStatus.NotInclude.Composition);
            if (eType == EPackage && cType == CGeneralisation)
                statuses.Add(EAStatus.NotInclude.Generalisation);
            if (eType == EPackage && cType == CRealisation)
                statuses.Add(EAStatus.NotInclude.Realization);

            return statuses;
        }

        public static List<IAlert> GetNotIncludeConnectorResult(string parent, EA.Element element , EA.Connector connector)
        {
            List<EAStatus.NotInclude> statuses = CheckNotIncludeConnector(element, connector);

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.NotInclude.Association)
                    alerts.Add(new Error(
                        $"({parent}) - S {GetElementTxt2(element)} {element.Name} s ID č. {element.ElementID} nesmí být spojena {TxtAssociation} s ID č. {connector.ConnectorID}."));
                if (status == EAStatus.NotInclude.Aggregation)
                    alerts.Add(new Error(
                        $"({parent}) - S {GetElementTxt2(element)} {element.Name} s ID č. {element.ElementID} nesmí být spojena {TxtAggregation} s ID č. {connector.ConnectorID}."));
                if (status == EAStatus.NotInclude.Composition)
                    alerts.Add(new Error(
                        $"({parent}) - S {GetElementTxt2(element)} {element.Name} s ID č. {element.ElementID} nesmí být spojena {TxtComposition} s ID č. {connector.ConnectorID}."));
                if (status == EAStatus.NotInclude.Realization)
                    alerts.Add(new Error(
                        $"({parent}) - S {GetElementTxt2(element)} {element.Name} s ID č. {element.ElementID} nesmí být spojena {TxtRealisation} s ID č. {connector.ConnectorID}."));
                if (status == EAStatus.NotInclude.Generalisation)
                    alerts.Add(new Error(
                        $"({parent}) - S {GetElementTxt2(element)} {element.Name} s ID č. {element.ElementID} nesmí být spojena {TxtGeneralisation} s ID č. {connector.ConnectorID}."));
            }
            return alerts;
        }

        /*
         * Not iclude connector 2
         */
        private static List<EAStatus.NotInclude> CheckNotIncludeConnector(EA.Element element1,
            EA.Element element2, EA.Connector connector)
        {
            List<EAStatus.NotInclude> statuses = new List<EAStatus.NotInclude>();
            string eType1 = element1.Type.ToLower();
            string eType2 = element2.Type.ToLower();
            string cType = connector.Type.ToLower();

            if (eType1 == EInterface &&
                eType2 == EInterface &&
                cType == CRealisation)
                statuses.Add(EAStatus.NotInclude.Realization);

            if (eType1 == EClass &&
                eType2 == EClass &&
                cType == CRealisation)
                statuses.Add(EAStatus.NotInclude.Realization);

            if (eType1 == EInterface &&
                eType2 == EClass &&
                cType == CRealisation)
                statuses.Add(EAStatus.NotInclude.Direction);


            return statuses;
        }

        public static List<IAlert> GetNotIncludeConnectorResult(string parent, EA.Element element1, EA.Element element2, EA.Connector connector)
        {
            if (!IsSouserceElement(element1, connector))
            {
                EA.Element tmp = element1;
                element1 = element2;
                element2 = tmp;
            }
            List<EAStatus.NotInclude> statuses = CheckNotIncludeConnector(element1, element2, connector);
        
            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.NotInclude.Realization)
                    alerts.Add(new Error(
                        $"({parent}) - Mezi {GetElementTxt2(element1)} {element1.Name} a {GetElementTxt2(element2)} {element2.Name} nesmí být relace s ID č. {connector.ConnectorID} typu {TxtRealisation}."));
                if (status == EAStatus.NotInclude.Direction)
                    alerts.Add(new Error(
                        $"({parent}) - Mezi {GetElementTxt2(element1)} {element1.Name} a {GetElementTxt2(element2)} {element2.Name} je relace {TxtRealisation} s ID č. {connector.ConnectorID} ve špatném směru."));
            }
            return alerts;
        }

        /*
         * Not iclude connector 3
         */
        private static List<EAStatus.NotInclude> CheckNotIncludeConnector(EA.Connector connector, Type diagramType)
        {
            List<EAStatus.NotInclude> statuses = new List<EAStatus.NotInclude>();

            if (diagramType == Type.Domain &&
                connector.Type.ToLower() == CAggregation &&
                connector.SupplierEnd.Aggregation == 1)
                statuses.Add(EAStatus.NotInclude.Aggregation);
            if (diagramType == Type.Domain &&
                connector.Type.ToLower() == CAggregation &&
                connector.SupplierEnd.Aggregation == 2)
                statuses.Add(EAStatus.NotInclude.Composition);
            if (diagramType == Type.Domain &&
                connector.Type.ToLower() == CRealisation)
                statuses.Add(EAStatus.NotInclude.Realization);
            if (diagramType == Type.Domain &&
                connector.Type.ToLower() == CGeneralisation)
                statuses.Add(EAStatus.NotInclude.Generalisation);

            return statuses;
        }

        public static List<IAlert> GetNotIncludeConnectorResult(string parent, EA.Connector connector, Type diagramType)
        {
            List<EAStatus.NotInclude> statuses = CheckNotIncludeConnector(connector, diagramType);

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.NotInclude.Aggregation)
                    alerts.Add(new Error(
                        $"({parent}) - Relace {TxtAggregation} s ID č. {connector.ConnectorID} nemá být v analytickém modelu tříd."));
                if (status == EAStatus.NotInclude.Composition)
                    alerts.Add(new Error(
                        $"({parent}) - Relace {TxtComposition} s ID č. {connector.ConnectorID} nemá být v analytickém modelu tříd."));
                if (status == EAStatus.NotInclude.Realization)
                    alerts.Add(new Error(
                        $"({parent}) - Relace {TxtRealisation} s ID č. {connector.ConnectorID} nemá být v analytickém modelu tříd."));
                if (status == EAStatus.NotInclude.Generalisation)
                    alerts.Add(new Error(
                        $"({parent}) - Relace {TxtGeneralisation} s ID č. {connector.ConnectorID} nemá být v analytickém modelu tříd."));
            }
            return alerts;
        }

        /*
         *  Not include Association class
         */
        private static List<EAStatus.NotInclude> CheckNotIncludeAssociationClass(EA.Element element)
        {
            List<EAStatus.NotInclude> statuses = new List<EAStatus.NotInclude>();

            if (element.Type.ToLower() == EClass && element.IsAssociationClass())
                statuses.Add(EAStatus.NotInclude.AssociationClass);

            return statuses;
        }

        public static List<IAlert> GetNotIncludeAssociationClassResult(string parent, EA.Element element)
        {
            List<EAStatus.NotInclude> statuses = CheckNotIncludeAssociationClass(element);

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.NotInclude.AssociationClass)
                    alerts.Add(new Error(
                        $"({parent}) - Asociační třída {element.Name} s ID č. {element.ElementID} nesmí být součástí návrhového modelu tříd."));
            }
            return alerts;
        }

        /*
         * Not include interface
         */
        private static List<EAStatus.NotInclude> CheckNotIncludeInterface(EA.Element element)
        {
            List<EAStatus.NotInclude> statuses = new List<EAStatus.NotInclude>();

            if (element.Type.ToLower() == EInterface)
                statuses.Add(EAStatus.NotInclude.Interface);

            return statuses;
        }

        public static List<IAlert> GetNotIncludeInterfaceResult(string parent, EA.Element element)
        {
            List<EAStatus.NotInclude> statuses = CheckNotIncludeInterface(element);

            List<IAlert> alerts = new List<IAlert>();
            foreach (var status in statuses)
            {
                if (status == EAStatus.NotInclude.Interface)
                    alerts.Add(new Error(
                        $"({parent}) - Rozhraní {element.Name} s ID č. {element.ElementID} nesmí být součástí analytického modelu tříd."));
            }
            return alerts;
        }
    }
}
