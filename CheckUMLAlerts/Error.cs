﻿using System.Drawing;

namespace CheckUMLAlerts
{
    public class Error : IAlert
    {
        public Error(string report)
        {
            Report = report;
        }

        public Image Icon { get; } = new Bitmap(SystemIcons.Error.ToBitmap(), new Size(18, 18));
        public string Report { get; set; }
    }
}
