﻿using System.Drawing;

namespace CheckUMLAlerts
{
    public class Warning : IAlert
    {
        public Warning(string report)
        {
            Report = report;
        }

        public Image Icon { get; } = new Bitmap(SystemIcons.Warning.ToBitmap(), new Size(18, 18));
        public string Report { get; set; }
    }
}
