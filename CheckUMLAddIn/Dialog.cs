﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using KontrolaUML;

namespace CheckUMLAddIn
{
    public partial class Dialog : Form
    {
        public Dialog(List<IEADiagram> eaLogicalDiagrams)
        {
            InitializeComponent(eaLogicalDiagrams);
        }

        private void ResizeDialogByLogicalDiagramsTable(object sender, EventArgs e)
        {
            ClientSize = new System.Drawing.Size(logicalDiagramsTable.Width + 24, 261);
        }

        private void LoadingLogicalDiagramsTable(object sender, EventArgs e)
        {
            logicalDiagramsTable.Columns[0].ReadOnly = true;
            logicalDiagramsTable.Columns[1].ReadOnly = true;
            logicalDiagramsTable.Columns[2].ReadOnly = true;
            Dictionary<string, string> comboBoxItems = new Dictionary<string, string>();
            int comboBoxMinWidth = 0;
            foreach (string enumElement in Enum.GetNames(typeof(KontrolaUML.Type)))
            {
                comboBoxItems[enumElement] = GetDescriptionByEnum((KontrolaUML.Type) Enum.Parse(typeof(KontrolaUML.Type), enumElement));
                comboBoxMinWidth = Math.Max(comboBoxMinWidth,
                    TextRenderer.MeasureText(comboBoxItems[enumElement], logicalDiagramsTable.Font).Width);
            }
            DataGridViewComboBoxCell comboBoxType = new DataGridViewComboBoxCell();
            comboBoxType.ValueType = typeof(string);
            comboBoxType.DataSource = new BindingSource(comboBoxItems, null);
            comboBoxType.DisplayMember = "Value";
            comboBoxType.ValueMember = "Key";
            DataGridViewColumn columnType = new DataGridViewColumn(comboBoxType);
            logicalDiagramsTable.Columns.Add(columnType);
            logicalDiagramsTable.Columns[4].HeaderText = logicalDiagramsTable.Columns[3].HeaderText;
            logicalDiagramsTable.Columns[3].Visible = false;
            foreach (DataGridViewRow row in logicalDiagramsTable.Rows)
            {
                row.Cells[4].Value = row.Cells[3].Value.ToString();
            }

            logicalDiagramsTable.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            comboBoxMinWidth += SystemInformation.VerticalScrollBarWidth + 3;
            logicalDiagramsTable.Columns[4].MinimumWidth = comboBoxMinWidth;
        }

        private void ResizeLogicalDiagramsTable(object sender, EventArgs e)
        {
            int logicalDiagramsTableWidth = 0;
            for (int i = 0; i < logicalDiagramsTable.ColumnCount; i++)
            {
                if (logicalDiagramsTable.Columns[i].Visible)
                {
                    logicalDiagramsTableWidth += logicalDiagramsTable.Columns[i].Width;
                    logicalDiagramsTableWidth++;
                }
            }
            logicalDiagramsTableWidth += SystemInformation.VerticalScrollBarWidth;

            logicalDiagramsTable.Size = new System.Drawing.Size(logicalDiagramsTableWidth, 208);
        }

        private static string GetDescriptionByEnum(Enum enumElement)
        {
            DescriptionAttribute[] description = (DescriptionAttribute[]) enumElement.GetType()
                .GetField(enumElement.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (description.Length > 0)
                return description[0].Description;
            return enumElement.ToString();
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in logicalDiagramsTable.Rows)
            {
                item.Cells[3].Value = Enum.Parse(typeof(KontrolaUML.Type), item.Cells[4].Value.ToString());
            }
            Close();
        }
    }
}
