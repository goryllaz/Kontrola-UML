﻿using System;
using System.ComponentModel;

namespace KontrolaUML
{
    public enum Type
    {
        [Description("-")]
        None,
        [Description("Návrhový model tříd")]
        Class,
        [Description("Analytický model tříd")]
        Domain
    }
    public interface IEADiagram
    {
        [DisplayName("Název")]
        string Name { get; }
        [DisplayName("ID")]
        int Id { get; }
        [DisplayName("GUID")]
        Guid Guid { get; }
        [DisplayName("Typ")]
        Type Type { get; set; }
    }
}
