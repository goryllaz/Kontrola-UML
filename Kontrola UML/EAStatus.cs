﻿namespace KontrolaUML
{
    class EAStatus
    {
        public enum Text
        {
            WhiteSpace,
            Delimiter,
            Diacritic,
            FirstUppercase,
            FirstLowercase
        }

        public enum Include
        {
            Type,
            Multiplicity
        }

        public enum NotInclude
        {
            Multiplicity,
            Attributes,
            Association,
            Aggregation,
            Composition,
            Dependency,
            Realization,
            Generalisation,
            AssociationClass,
            Direction,
            Interface
        }
    }
}
