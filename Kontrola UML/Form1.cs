﻿using System;
using System.Windows.Forms;

namespace KontrolaUML
{
    public partial class Form1 : Form
    {
        private FileReader FileReader { get; set; }

        private EAHandler EAHander { get; set; }

        public Form1()
        {
            InitializeComponent();
            FileReader = new FileReader();
        }

        private void selectBtn_Click(object sender, EventArgs e)
        {
            if(selectFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileReader.Path = selectFileDialog.FileName;
                DisplayFileName(selectFileDialog.FileName);
                checkBtn.Enabled = true;
            }
        }

        private void DisplayFileName (string text)
        {
            filePath.Text = text;
        }

        private void checkBtn_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            EAHander = new EAHandler();
            EAHander.CheckProject(FileReader.Path);
            Cursor.Current = Cursors.Default;
            alertsTable.DataSource = EAHander.Alerts;
            chooseDiagram.Enabled = true;

        }

        private void chooseDiagram_Click(object sender, EventArgs e)
        {
            Dialog dialog = new Dialog(EAHander.EADiagrams);
            dialog.TopLevel = true;
            Enabled = false;
            dialog.Closed += CloseDialog;
            dialog.Show();
        }

        private void CloseDialog(object sender, EventArgs e)
        {
            Enabled = true;
            Cursor.Current = Cursors.WaitCursor;
            EAHander.CheckAllDiagrams();
            Cursor.Current = Cursors.Default;
            alertsTable.DataSource = EAHander.Alerts;
        }

        private void Form1Resize(object sender, EventArgs e)
        {
            chooseDiagram.Location = new System.Drawing.Point(Width - 114, 36);
            filePath.Width = Width-122;
            filePath.Location = new System.Drawing.Point(Width - (filePath.Width + 28), 16);
            alertsTable.Width = Width - 40;
            alertsTable.Height = Height - 116;
            checkBtn.Location = new System.Drawing.Point(Width / 2 - (checkBtn.Width / 2), 36);
        }

        private void AlertsTableResize(object sender, EventArgs e)
        {
            if (alertsTable.Columns.Count >= 2)
            {
                int alertsTableMinWidth = alertsTable.Width - 22 -
                                          SystemInformation.VerticalScrollBarWidth - 3;
                if (alertsTable.Columns[1].MinimumWidth < alertsTableMinWidth)
                {
                    alertsTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                    alertsTable.Columns[1].Width = alertsTableMinWidth;
                }
                else
                {
                    alertsTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                }
            }
        }

        private void AlertsTableLoading(object sender, EventArgs e)
        {
            if (alertsTable.Columns.Count >= 2)
            {
                alertsTable.Columns[0].ReadOnly = true;
                alertsTable.Columns[1].ReadOnly = true;
                alertsTable.Columns[1].MinimumWidth = alertsTable.Columns[1].Width;
                alertsTable.Columns[0].MinimumWidth = 22;
                alertsTable.Columns[0].Width = 22;
            }
        }
    }
}
