﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace KontrolaUML
{
    partial class Dialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(List<IEADiagram> eaDiagrams)
        {
            this.logicalDiagramsTable = new System.Windows.Forms.DataGridView();
            this.closeBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.logicalDiagramsTable)).BeginInit();
            this.SuspendLayout();
            // 
            // logicalDiagramsTable
            //
            var source = new BindingSource();
            source.DataSource = eaDiagrams;
            this.logicalDiagramsTable.DataSource = source;
            this.logicalDiagramsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.logicalDiagramsTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.logicalDiagramsTable.Location = new System.Drawing.Point(12, 12);
            this.logicalDiagramsTable.Name = "logicalDiagramsTable";
            this.logicalDiagramsTable.Size = new System.Drawing.Size(200, 208);
            this.logicalDiagramsTable.RowHeadersVisible = false;
            this.logicalDiagramsTable.TabIndex = 0;
            this.logicalDiagramsTable.AllowUserToResizeRows = false;
            this.logicalDiagramsTable.AllowUserToResizeColumns = false;
            this.logicalDiagramsTable.Resize += this.ResizeDialogByLogicalDiagramsTable;
            this.logicalDiagramsTable.CellValueChanged += ResizeLogicalDiagramsTable;
            // 
            // closeBtn
            // 
            this.closeBtn.Location = new System.Drawing.Point(12, 226);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(75, 23);
            this.closeBtn.TabIndex = 1;
            this.closeBtn.Text = "Uložit";
            this.closeBtn.UseVisualStyleBackColor = true;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // Dialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(logicalDiagramsTable.Width + 24, 261 );
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Controls.Add(this.closeBtn);
            this.Controls.Add(this.logicalDiagramsTable);
            this.Name = "Dialog";
            this.Text = "Diagramy";
            ((System.ComponentModel.ISupportInitialize)(this.logicalDiagramsTable)).EndInit();
            this.ResumeLayout(false);
            this.Load += LoadingLogicalDiagramsTable;
            this.Load += ResizeLogicalDiagramsTable;
        }

        #endregion

        private System.Windows.Forms.DataGridView logicalDiagramsTable;
        private System.Windows.Forms.Button closeBtn;
    }
}