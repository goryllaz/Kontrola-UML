﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using CheckUMLAlerts;
using CheckUMLExternalController;

namespace KontrolaUML
{
    public class EAHandler
    {
        private bool _isRepositoryOpen;
        private string _path;

        public EA.Repository Repository { set; get; }
        public List<IEADiagram> EADiagrams { set; get; }
        public List<IAlert> Alerts { set; get; }

        /*
         * Declaration of configuration for checking diagram
         */
        private bool _checkDiacriticElements;
        private bool _checkIncludingMultiplicity;
        private bool _checkNotIncludingMultiplicity;
        private bool _checkNotIncludingConnector;
        private bool _checkNotIncludingConnector2;
        private bool _checkCamelCaseElements;
        private bool _checkFirstUppercaseElements;
        private bool _checkCamelCaseMethods;
        private bool _checkDiacriticMethods;
        private bool _checkFirstLowercaseMethods;
        private bool _checkFirstUppercaseConstructor;
        private bool _checkIncludingReturnTypeMethods;
        private bool _checkCamelCaseAttributes;
        private bool _checkDiacriticAttributes;
        private bool _checkFirstLowercaseAttributes;
        private bool _checkIncludingTypeAttributes;
        private bool _checkNotIncludeAssociationClass;
        private bool _checkNotIncludingAttributes;
        private bool _checkNotIncludingInterfaces;




        public EAHandler()
        {
            Repository = new EA.RepositoryClass();
            EADiagrams = new List<IEADiagram>();
            _isRepositoryOpen = false;

            //Default setting of configuration
            _checkDiacriticElements = true;
            _checkIncludingMultiplicity = true;
            _checkNotIncludingMultiplicity = true;
            _checkNotIncludingConnector = true;
            _checkNotIncludingConnector2 = true;
            _checkCamelCaseElements = true;
            _checkFirstUppercaseElements = true;
            _checkCamelCaseMethods = true;
            _checkDiacriticMethods = true;
            _checkFirstLowercaseMethods = true;
            _checkFirstUppercaseConstructor = true;
            _checkIncludingReturnTypeMethods = true;
            _checkCamelCaseAttributes = true;
            _checkDiacriticAttributes = true;
            _checkFirstLowercaseAttributes = true;
            _checkIncludingTypeAttributes = true;
            _checkNotIncludeAssociationClass = true;
            _checkNotIncludingAttributes = true;
            _checkNotIncludingInterfaces = true;

            //Load configuration from app.conf file
            loadConfiguration();
        }

        ~EAHandler()
        {
            if (_isRepositoryOpen)
            Close();
        }

        private void loadConfiguration()
        {
            bool value;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkDiacriticElements"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkDiacriticElements"], out value))
                _checkDiacriticElements = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkIncludingMultiplicity"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkIncludingMultiplicity"], out value))
                _checkIncludingMultiplicity = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkNotIncludingMultiplicity"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkNotIncludingMultiplicity"], out value))
                _checkNotIncludingMultiplicity = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkNotIncludingConnector"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkNotIncludingConnector"], out value))
                _checkNotIncludingConnector = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkNotIncludingConnector2"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkNotIncludingConnector2"], out value))
                _checkNotIncludingConnector2 = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkCamelCaseElements"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkCamelCaseElements"], out value))
                _checkCamelCaseElements = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkFirstUppercaseElements"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkFirstUppercaseElements"], out value))
                _checkFirstUppercaseElements = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkCamelCaseMethods"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkCamelCaseMethods"], out value))
                _checkCamelCaseMethods = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkDiacriticMethods"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkDiacriticMethods"], out value))
                _checkDiacriticMethods = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkFirstLowercaseMethods"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkFirstLowercaseMethods"], out value))
                _checkFirstLowercaseMethods = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkFirstUppercaseConstructor"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkFirstUppercaseConstructor"], out value))
                _checkFirstUppercaseConstructor = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkIncludingReturnTypeMethods"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkIncludingReturnTypeMethods"], out value))
                _checkIncludingReturnTypeMethods = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkCamelCaseAttributes"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkCamelCaseAttributes"], out value))
                _checkCamelCaseAttributes = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkDiacriticAttributes"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkDiacriticAttributes"], out value))
                _checkDiacriticAttributes = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkFirstLowercaseAttributes"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkFirstLowercaseAttributes"], out value))
                _checkFirstLowercaseAttributes = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkIncludingTypeAttributes"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkIncludingTypeAttributes"], out value))
                _checkIncludingTypeAttributes = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkNotIncludeAssociationClass"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkNotIncludeAssociationClass"], out value))
                _checkNotIncludeAssociationClass = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkNotIncludingAttributes"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkNotIncludingAttributes"], out value))
                _checkNotIncludingAttributes = value;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["checkNotIncludingInterfaces"]) &&
                bool.TryParse(ConfigurationManager.AppSettings["checkNotIncludingInterfaces"], out value))
                _checkNotIncludingInterfaces = value;
    }

        private bool Open(string path)
        {
            _isRepositoryOpen = true;
            return Repository.OpenFile(path);
        }

        private void Close()
        {
            _isRepositoryOpen = false;
            Repository.CloseFile();
        }

        private void LoadingStackPackages(Stack<EA.Package> stackPackage, EA.Package package)
        {
            for (int j = 0; j < package.Packages.Count; j++)
            {
                stackPackage.Push((EA.Package)package.Packages.GetAt((short)j));
            }
        }

        private bool IsClassModel(EA.Diagram diagram)
        {
            if (IsLogicalDiagram(diagram) && (
                diagram.Name.ToLower().Contains("class") ||
                diagram.Name.ToLower().Contains("system") ||
                diagram.Name.ToLower().Contains("navrhovy") ||
                diagram.Name.ToLower().Contains("návrhový")))
            {
                return true;
            }

            return false;
        }

        private bool IsDomainModel(EA.Diagram diagram)
        {
            if (IsLogicalDiagram(diagram) &&
                diagram.Name.ToLower().Contains("domain") ||
                diagram.Name.ToLower().Contains("analytic") ||
                diagram.Name.ToLower().Contains("analyticky") ||
                diagram.Name.ToLower().Contains("analytický"))
            {
                return true;
            }

            return false;
        }

        private bool IsLogicalDiagram(EA.Diagram diagram)
        {
            if (diagram.Type.ToLower() == "logical")
            {
                return true;
            }

            return false;
        }

        private Type GetDiagramType(EA.Diagram diagram)
        {
            if (IsClassModel(diagram))
                return Type.Class;
            if (IsDomainModel(diagram))
                return Type.Domain;
            return Type.None;
        }

        private void LoadingListEaLogicalDiagrams(EA.Package package)
        {
            if ( package.Diagrams.Count == 0 ) return;
            for(int i = 0; i < package.Diagrams.Count; i++)
            {
                EA.Diagram diagram = (EA.Diagram) package.Diagrams.GetAt((short)i);
                if ( IsLogicalDiagram(diagram) )
                {
                    EADiagrams.Add(new EALogicalDiagram(diagram, GetDiagramType(diagram)));
                }
            }
        }

        private void FindLogicalDiagrams()
        {
            for ( short i = 0; i < Repository.Models.Count; i++ )
            {
                EA.Package model = (EA.Package) Repository.Models.GetAt(i);
                if (model.Packages.Count > 0)
                {
                    Stack<EA.Package> stackPackages  = new Stack<EA.Package>();
                    LoadingStackPackages(stackPackages, model);

                    while ( stackPackages.Count != 0 )
                    {
                        EA.Package package = stackPackages.Pop();
                        LoadingListEaLogicalDiagrams(package);
                        LoadingStackPackages(stackPackages, package);
                    }
                }
            }
        }

        private void FindElementsOfDiagrams()
        {
            foreach (EALogicalDiagram eaLogicalDiagram in EADiagrams)
            {
                List<EA.DiagramObject> diagramObjects = eaLogicalDiagram.GetDiagramObjects();
                foreach (EA.DiagramObject diagramObject in diagramObjects)
                {
                    if (Repository.GetElementByID(
                        diagramObject.ElementID).Type.ToLower() == "class" ||
                        Repository.GetElementByID(
                            diagramObject.ElementID).Type.ToLower() == "interface" ||
                        Repository.GetElementByID(
                            diagramObject.ElementID).Type.ToLower() == "package")
                    {
                        eaLogicalDiagram.AddElement(
                            Repository.GetElementByID(diagramObject.ElementID));
                    }
                }
            }
        }

        private void FindRelationsOfDiagrams()
        {
            foreach (EALogicalDiagram eaLogicalDiagram in EADiagrams)
            {
                eaLogicalDiagram.CreateConnectionsArrayList();
                for (int i = 0; i < eaLogicalDiagram.CountElements; i++)
                {
                    for (short j = 0; j < eaLogicalDiagram.GetElement(i).Connectors.Count; j++)
                    {
                        EA.Connector connector = (EA.Connector) eaLogicalDiagram.GetElement(i).Connectors.GetAt(j);
                        int elementId1 = 0;
                        int elementId2 = 0;
                        for (int k = 0; k < eaLogicalDiagram.CountElements; k++)
                        {
                            if (eaLogicalDiagram.GetElement(k).ElementID == connector.ClientID)
                            {
                                elementId1 = k;
                            }
                            if (eaLogicalDiagram.GetElement(k).ElementID == connector.SupplierID)
                            {
                                elementId2 = k;
                            }
                        }
                        if (eaLogicalDiagram.GetConnectors(elementId1, elementId2) == null &&
                            eaLogicalDiagram.GetConnectors(elementId2, elementId1) == null)
                        {
                            List<EA.Connector> connectorList = new List<EA.Connector>();
                            connectorList.Add(connector);
                            eaLogicalDiagram.AddConnectors(connectorList, elementId1, elementId2);
                            eaLogicalDiagram.AddConnectors(connectorList, elementId2, elementId1);
                        }
                        else
                        {
                            bool isConnectorExist = false;
                            for (int k = 0; k < eaLogicalDiagram.GetConnectors(elementId1, elementId2).Count; k++)
                            {
                                if (eaLogicalDiagram.GetConnectors(elementId1, elementId2)[k].ConnectorID == connector.ConnectorID &&
                                    eaLogicalDiagram.GetConnectors(elementId2, elementId1)[k].ConnectorID == connector.ConnectorID)
                                {
                                    isConnectorExist = true;
                                }
                            }
                            if (!isConnectorExist)
                            {
                                eaLogicalDiagram.GetConnectors(elementId1, elementId2).Add(connector);
                            }
                        }
                    }
                }
            }
        }

        public void CheckAllDiagrams(bool isCallFromAddIn)
        {
            if (!isCallFromAddIn && !_isRepositoryOpen)
                if (!Open(_path)) return;
            Alerts = new List<IAlert>();
            foreach (IEADiagram diagram in EADiagrams)
            {
                if (diagram.Type == Type.Class || diagram.Type == Type.Domain)
                {
                    CheckLogicalDiagram((EALogicalDiagram) diagram);
                }
            }

            Alerts.AddRange(GetResultsExternalControllers());

            if ( !isCallFromAddIn && _isRepositoryOpen)
                Close();
        }

        public void CheckAllDiagrams()
        {
            CheckAllDiagrams(false);
        }

        public void CheckLogicalDiagram(EALogicalDiagram diagram)
        {

            for (int i = 0; i < diagram.CountElements; i++)
            {
                EA.Element element = diagram.GetElement(i);
                // Check element (class, interface, package)
                if (element.Type != null &&
                    (element.Type.ToLower() == "class" ||
                     element.Type.ToLower() == "interface" ||
                     element.Type.ToLower() == "package"))
                {
                    // - Diacritic name
                    if(_checkDiacriticElements)
                        Alerts.AddRange(EAController.GetTextDiacriticResult(GetParent(diagram), element));
                    for (int j = i; j < diagram.CountElements; j++)
                    {
                        // -- Connectors
                        List<EA.Connector> connectors = diagram.GetConnectors(i, j);
                        if (connectors != null)
                            foreach (EA.Connector connector in connectors)
                            {
                                // -- Association, aggregation
                                if (connector.Type.ToLower() == "association" ||
                                    connector.Type.ToLower() == "aggregation")
                                {
                                    // --- Include multiplicity
                                    if (_checkIncludingMultiplicity)
                                        Alerts.AddRange(EAController.GetIcludeMultiplicityResult(GetParent(diagram),
                                            element,
                                            diagram.GetElement(j),
                                            connector));
                                }
                                // -- Nesting, import, dependency
                                if (connector.Type.ToLower() == "nesting" ||
                                    connector.Type.ToLower() == "realisation" ||
                                    connector.Type.ToLower() == "usage" ||
                                    connector.Type.ToLower() == "package" ||
                                    connector.Type.ToLower() == "dependency" ||
                                    connector.Type.ToLower() == "usage" ||
                                    connector.Type.ToLower() == "trace")
                                {
                                    // --- Not include multiplicity
                                    if (_checkNotIncludingMultiplicity)
                                        Alerts.AddRange(EAController.GetNotIncludeMultiplicityResult(GetParent(diagram),
                                            element,
                                            diagram.GetElement(j),
                                            connector));
                                }
                                // -- Not include connector
                                if (_checkNotIncludingConnector)
                                    Alerts.AddRange(EAController.GetNotIncludeConnectorResult(GetParent(diagram),
                                        element,
                                        connector));
                                if (_checkNotIncludingConnector)
                                    Alerts.AddRange(EAController.GetNotIncludeConnectorResult(GetParent(diagram),
                                        diagram.GetElement(j),
                                        connector));
                                if (_checkNotIncludingConnector2)
                                    Alerts.AddRange(EAController.GetNotIncludeConnectorResult(GetParent(diagram),
                                        element,
                                        diagram.GetElement(j),
                                        connector));
                            }
                    }
                }
                    // Check element (class, interface)
                    if (element.Type != null &&
                    (element.Type.ToLower() == "class" ||
                     element.Type.ToLower() == "interface"))
                {
                    // - CamelCase name
                    if (_checkCamelCaseElements)
                        Alerts.AddRange(EAController.GetCamelCaseResult(GetParent(diagram), element));
                    // - First Uppercase name
                    if (_checkFirstUppercaseElements)
                        Alerts.AddRange(EAController.GetFirstUppercaseResult(GetParent(diagram), element));
                    // - Check methods (class, interface)
                    for (short j = 0; j < element.Methods.Count; j++)
                    {
                        EA.Method method = (EA.Method)element.Methods.GetAt(j);
                        // -- CamelCase name
                        if (_checkCamelCaseMethods)
                            Alerts.AddRange(EAController.GetCamelCaseResult(GetParent(diagram, element), method));
                        // -- Diacritic name
                        if (_checkDiacriticMethods)
                            Alerts.AddRange(EAController.GetTextDiacriticResult(GetParent(diagram, element), method));
                        // -- First lowercases name
                        if (_checkFirstLowercaseMethods && method.Stereotype.ToLower() != "constructor")
                            Alerts.AddRange(EAController.GetFirstLowercaseResult(GetParent(diagram, element), method));
                        // -- First uppercase constructor
                        if (_checkFirstUppercaseConstructor && method.Stereotype.ToLower() == "constructor")
                            Alerts.AddRange(EAController.GetFirstUppercaseResult(GetParent(diagram, element), method));
                        // -- Include return type
                        if (_checkIncludingReturnTypeMethods)
                            Alerts.AddRange(EAController.GetIncludeTypeResult(GetParent(diagram, element), method));
                    }

                }
                if (element.Type != null &&
                    element.Type.ToLower() == "class")
                {
                    // - Check attributes (class)
                    for (short j = 0; j < element.Attributes.Count; j++)
                    {
                        EA.Attribute attribute = (EA.Attribute)element.Attributes.GetAt(j);
                        // -- CamelCase name
                        if (_checkCamelCaseAttributes)
                            Alerts.AddRange(EAController.GetCamelCaseResult(GetParent(diagram, element), attribute));
                        // -- Diacritic name
                        if (_checkDiacriticAttributes)
                            Alerts.AddRange(EAController.GetTextDiacriticResult(GetParent(diagram, element), attribute));
                        // -- First lowercase name
                        if (_checkFirstLowercaseAttributes)
                            Alerts.AddRange(EAController.GetFirstLowercaseResult(GetParent(diagram, element), attribute));
                        // -- Include attribute type
                        if (_checkIncludingTypeAttributes)
                            Alerts.AddRange(EAController.GetIncludeTypeResult(GetParent(diagram, element), attribute));
                    }
                    // - Check design class diagram
                    if (_checkNotIncludeAssociationClass && diagram.Type == Type.Class)
                        Alerts.AddRange(EAController.GetNotIncludeAssociationClassResult(GetParent(diagram), element));

                }
                if (element.Type != null &&
                    element.Type.ToLower() == "interface")
                {
                    // - Check interface not include attributes
                    if (_checkNotIncludingAttributes)
                    Alerts.AddRange(EAController.GetNotIncludeAttributeResult(GetParent(diagram), element));
                    // -- Check domain class diagram
                    if (_checkNotIncludingInterfaces && diagram.Type == Type.Domain)
                        Alerts.AddRange(EAController.GetNotIncludeInterfaceResult(GetParent(diagram), element));
                }
            }
        }

        private string GetParent(IEADiagram diagram)
        {
            return $"Diagram: {diagram.Name}";
        }

        private string GetParent(IEADiagram diagram, EA.Element element)
        {
            if (element.Type.ToLower() == "class")
                return $"Diagram: {diagram.Name}, Trida: {element.Name}";
            if (element.Type.ToLower() == "interface")
                return $"Diagram: {diagram.Name}, Rozhraní: {element.Name}";
            return $"Diagram: {diagram.Name}, Element: {element.Name}";
        }

        private List<IAlert> GetResultsExternalControllers()
        {
            string path = @"ExternalControllers\";
            List<IAlert> alerts = new List<IAlert>();
            if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + path))
            {
                string[] nameControllers = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory+path, "*.dll");
                List<Assembly> assemblies = new List<Assembly>();
                foreach (string nameController in nameControllers)
                {
                    AssemblyName assemblyName = AssemblyName.GetAssemblyName(nameController);
                    assemblies.Add(Assembly.Load(assemblyName));
                }

                System.Type typeController = typeof(IEAExternalController);
                List<System.Type> typeControllers = new List<System.Type>();
                foreach (Assembly assembly in assemblies)
                {
                    if (assembly != null)
                    {
                        System.Type[] types = assembly.GetTypes();
                        foreach (System.Type type in types)
                        {
                            if (type.IsAbstract || type.IsInterface)
                                continue;
                            if (type.GetInterface(typeController.FullName) != null)
                                typeControllers.Add(type);
                        }
                    }
                }

                List<IEAExternalController> controllers = new List<IEAExternalController>();
                foreach (System.Type type in typeControllers)
                {
                    controllers.Add((IEAExternalController) Activator.CreateInstance(type));
                }

                foreach (IEAExternalController controller in controllers)
                {
                    controller.Repository = Repository;
                    alerts.AddRange(controller.GetResults());
                }
            }

            return alerts;
        }

        public void CheckProject(string path)
        {
            _path = path;
            if (Open(path))
            {
                FindLogicalDiagrams();
                FindElementsOfDiagrams();
                FindRelationsOfDiagrams();
                CheckAllDiagrams();
            }
            if(_isRepositoryOpen)
                Close();
        }

        public void CheckProject(EA.Repository repository)
        {
            Repository = repository;
            _isRepositoryOpen = true;
            FindLogicalDiagrams();
            FindElementsOfDiagrams();
            FindRelationsOfDiagrams();
            CheckAllDiagrams(true);
            _isRepositoryOpen = false;
        }
    }
}